# Getting Started

### Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.2.6.RELEASE/maven-plugin/)
* [Spring Data Reactive for Apache Cassandra](https://docs.spring.io/spring-boot/docs/2.2.6.RELEASE/reference/htmlsingle/#boot-features-cassandra)

### What does this contains?
* ReactiveCassandraRepository
* CassandraRepository
* Cassandra table with composite keys 

### Cassandra table creation script:
<pre>
CREATE TABLE "AC_MsisdnActivity" (
  key text,
  key2 bigint,
  column1 bigint,
  column2 text,
  value text,
  PRIMARY KEY ((key, key2), column1, column2)
);

describe Table "AC_MsisdnActivity";

drop table "AC_MsisdnActivity";
</pre>
