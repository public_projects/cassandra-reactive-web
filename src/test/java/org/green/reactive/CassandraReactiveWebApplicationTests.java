package org.green.reactive;

import org.green.reactive.repository.ACMsisdnOwner2ReactiveRepository;
import org.green.reactive.repository.AcMsisdnActivityReactiveRepository;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.cassandra.core.cql.CqlTemplate;
import java.time.Duration;
import java.util.concurrent.atomic.AtomicLong;

@SpringBootTest
class CassandraReactiveWebApplicationTests {
	private static final Logger logger = LoggerFactory.getLogger(CassandraReactiveWebApplicationTests.class);

	@Autowired
	private AcMsisdnActivityReactiveRepository acMsisdnActivityReactiveRepository;

	@Autowired
	private ACMsisdnOwner2ReactiveRepository aCMsisdnOwner2ReactiveRepository;

	@Test
	void contextLoads() {
//		logger.info("AC_MsisdnActivity. count: {}", acMsisdnActivityReactiveRepository.count().block());

		AtomicLong atomicLong = new AtomicLong();

		acMsisdnActivityReactiveRepository.findAll().toStream().forEach(acMsisdnActivityEntity -> {
			long currentCount = atomicLong.incrementAndGet();
			if (currentCount % 100000 == 0) {
				logger.info("count: {}", currentCount);
			}
		});
	}

	@Test
	void checkOwnershipData(){
		logger.info("AC_OwnerMsisdn_v2. count: {}", aCMsisdnOwner2ReactiveRepository.count().block(Duration.ofMinutes(10)));
		AtomicLong atomicLong = new AtomicLong();
		aCMsisdnOwner2ReactiveRepository.findAll().toStream(10000).forEach(acMsisdnOwner2Entity -> {
			long currentCount = atomicLong.incrementAndGet();
			if (currentCount % 100000 == 0) {
				logger.info("AC_OwnerMsisdn_v2. count: {}", currentCount);
			}
		});
	}
}
