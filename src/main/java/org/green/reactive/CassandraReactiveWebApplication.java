package org.green.reactive;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CassandraReactiveWebApplication {
	public static void main(String[] args) {
		SpringApplication.run(CassandraReactiveWebApplication.class, args);
	}
}
