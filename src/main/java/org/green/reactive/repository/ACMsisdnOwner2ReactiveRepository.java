package org.green.reactive.repository;

import org.green.reactive.entity.ACMsisdnActivityEntity;
import org.green.reactive.entity.ACMsisdnOwner2Entity;
import org.springframework.data.cassandra.repository.ReactiveCassandraRepository;
import org.springframework.stereotype.Repository;
import java.io.Serializable;

@Repository
public interface ACMsisdnOwner2ReactiveRepository extends ReactiveCassandraRepository<ACMsisdnOwner2Entity, Serializable> {}
