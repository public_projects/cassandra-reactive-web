package org.green.reactive.repository;

import org.green.reactive.entity.ACMsisdnActivityEntity;
import org.springframework.data.cassandra.repository.AllowFiltering;
import org.springframework.data.cassandra.repository.ReactiveCassandraRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

import java.io.Serializable;
import java.util.stream.Stream;


@Repository
public interface AcMsisdnActivityReactiveRepository extends ReactiveCassandraRepository<ACMsisdnActivityEntity, Serializable> {
    @AllowFiltering
    Flux<ACMsisdnActivityEntity> findAllByKeyAndKey2(String key, Long key2);

    @AllowFiltering
    Stream<ACMsisdnActivityEntity> findByKeyAndKey2(String key, Long key2);
}
