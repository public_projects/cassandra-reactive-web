package org.green.reactive.repository;

import org.green.reactive.entity.ACMsisdnActivityEntity;
import org.springframework.data.cassandra.repository.AllowFiltering;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;


@Repository
public interface AcMsisdnActivityRepository extends CassandraRepository<ACMsisdnActivityEntity, Serializable> {
    @AllowFiltering
    List<ACMsisdnActivityEntity> findAllByKeyAndKey2(String key, Long key2);
}
