package org.green.reactive.controller;

import org.green.reactive.entity.ACMsisdnActivityEntity;
import org.green.reactive.repository.AcMsisdnActivityReactiveRepository;
import org.green.reactive.repository.AcMsisdnActivityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.cassandra.core.CassandraTemplate;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("msisdnActivity")
public class AcMsisdnActivityController {

    @Autowired
    private AcMsisdnActivityReactiveRepository acMsisdnActivityReactiveRepository;

    @Autowired
    private AcMsisdnActivityRepository acMsisdnActivityRepository;

    @GetMapping(value = "/all/flux", produces = MediaType.APPLICATION_JSON_VALUE)
    public Flux<ACMsisdnActivityEntity> getAllMsisdnActivityFlux() {
//        return acMsisdnActivityReactiveRepository.findAllByKeyAndKey2("60089208263", 202002L);
        return acMsisdnActivityReactiveRepository.findAll();
    }

    @GetMapping(value = "/count", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<Long> getAllMsisdnActivityCountFlux() {
//        return acMsisdnActivityReactiveRepository.findAllByKeyAndKey2("60089208263", 202002L);
        return acMsisdnActivityReactiveRepository.count();
    }

    @GetMapping("/all/blocking")
    public List<ACMsisdnActivityEntity> getAllMsisdnActivityBlocking() {
        return acMsisdnActivityRepository.findAllByKeyAndKey2("60089208263", 202002L);
    }

    @GetMapping("/all/limit")
    public List<ACMsisdnActivityEntity> getAllMsisdnActivityStreamLimit() {
        return acMsisdnActivityReactiveRepository.findByKeyAndKey2("60089208263", 202002L).limit(10).collect(Collectors.toList());
    }
}
