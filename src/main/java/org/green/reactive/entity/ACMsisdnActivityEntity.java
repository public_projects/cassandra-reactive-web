package org.green.reactive.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.*;

import java.io.Serializable;

@Getter
@Setter
@Table(value = "AC_MsisdnActivity", forceQuote = true)
public class ACMsisdnActivityEntity implements Serializable {
        @PrimaryKeyColumn(name = "key", ordinal = 0, type = PrimaryKeyType.PARTITIONED)
        private String key;
        @PrimaryKeyColumn(name = "key2", ordinal = 1, type = PrimaryKeyType.CLUSTERED)
        private String key2;
        @Column("column1")
        private String column1;
        @Column("column2")
        private String column2;
        @Column("value")
        private String value;
}
