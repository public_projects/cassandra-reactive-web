package org.green.reactive.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;
import java.io.Serializable;

@Getter
@Setter
@Table(value = "AC_OwnerMsisdn_v2", forceQuote = true)
public class ACMsisdnOwner2Entity implements Serializable {
    @PrimaryKeyColumn(name = "key", ordinal = 0, type = PrimaryKeyType.PARTITIONED)
    private String key;
    @PrimaryKeyColumn(name = "key2", ordinal = 1, type = PrimaryKeyType.CLUSTERED)
    private String key2;
    @Column("column1")
    private String column1;
    @Column("value")
    private String value;
}
