package org.green.reactive.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
//import org.springframework.data.cassandra.config.CqlSessionFactoryBean;
import org.springframework.data.cassandra.config.CqlSessionFactoryBean;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {
//    @Bean
//    public CqlSessionFactoryBean session() {
//        CqlSessionFactoryBean session = new CqlSessionFactoryBean();
//        session.setContactPoints("localhost");
//        session.setKeyspaceName("ipf");
//        session.setPort(9042);
////        session.setLocalDatacenter("datacenter1");
//
//        return session;
//    }

    @Bean
    public CqlSessionFactoryBean session() {

        CqlSessionFactoryBean session = new CqlSessionFactoryBean();
        session.setContactPoints("localhost");
        session.setKeyspaceName("ipf");
        session.setPort(9042);
        session.setLocalDatacenter("datacenter1");

        return session;
    }
}
